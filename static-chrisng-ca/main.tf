provider "aws" {
  region = var.region
}

module "vpc_networking" {
  source = "../modules/vpc_networking"
  region = var.region
  vpc_cidr_block = var.vpc_cidr_block
  public_subnet_cidr_block = var.public_subnet_cidr_block
  private_subnet_cidr_block = var.private_subnet_cidr_block
}

module "s3-website" {
  source = "../modules/s3-website"
  bucket_name = var.bucket_name
}

resource "aws_route53_zone" "static_zone" {
  name = "static.chrisng.ca."
}

# resource "aws_route53_record" "static_zone_ns" {
#   zone_id = aws_route53_zone.static_zone.zone_id
#   name = "static.chrisng.ca"
#   type = "NS"
#   records = [
#     "kurt.ns.cloudflare.com",
#     "zoe.ns.cloudflare.com"
#   ] 
#   ttl = "86400"
#   allow_overwrite = true
# }

# resource "aws_route53_record" "static_zone_ns_default" {
#   zone_id = aws_route53_zone.static_zone.zone_id
#   name = "static.chrisng.ca"
#   type = "NS"
#   records = [
#     "kurt.ns.cloudflare.com",
#     "zoe.ns.cloudflare.com"
#   ] 
#   ttl = "86400"
# }

output "region" {
  value = var.region
}

output "website_endpoint" {
  value = module.s3-website.website_endpoint
}

output "vpc_id" {
  value = module.vpc_networking.vpc_id
}

output "vpc_cidr" {
  value = module.vpc_networking.vpc_cidr
}

output "public_subnet_cidr" {
  value = module.vpc_networking.public_subnet_cidr
}

output "private_subnet_cidr" {
  value = module.vpc_networking.private_subnet_cidr
}

terraform {
  backend "s3" {
    bucket = "remote-s3-backend-bucket"
    key = "remote-state-key.tfstates"
    region = "ca-central-1"
  }
}
