# Terraform S3 Website

Serve static web sites from AWS S3 buckets, managed by a [Terraform](https://www.terraform.io/) plan.

Based on Packt [Learn Terraform for Cloud Infrastructures](https://www.packtpub.com/product/learn-terraform-for-cloud-infrastructures-video/9781838982959)

* [Related Github repo](https://github.com/PacktPublishing/Learn-Terraform-for-Cloud-Infrastructures)

## Features

Modules for:

* AWS VPC
* AWS S3 static web sites

## Usage

Set environment variable "AWS_PROFILE". From "static-chrisng-ca" folder:

```
terraform init
terraform plan -var-file=<ENVIRONMENT>.tfvars
terraform apply
```

## Variables

### Example: production.tfvars

```
region = "ca-central-1"
bucket_name = "static.chrisng.ca"
... etc
```